/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar LabeledExpr;


/** The start rule; begin parsing here. */
prog:   stat+ ; 

stat: expr NEWLINE      # printExpr
| ID '=' expr NEWLINE   # assign
| NEWLINE               # blank
;

expr: expr op=('*'|'/') expr    # MulDiv
| expr op=('+'|'-') expr        # AddSub
| INT                           # int
| ID                            # id
| '(' expr ')'                  # parens
;

NEWLINE : [\r\n]+ ;

INT     : [0-9]+ ;

ID      : [a-z]+ ;

MUL : '*' ; // assigns token name to '*' used above in grammar
DIV : '/' ;
ADD : '+' ;
SUB : '-' ;
ATR : '=' ;
LPR : '(' ;
RPR : ')' ;